/**
 * @file
 * TestThreeJS behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Behavior description.
   */
  Drupal.behaviors.ThreeJSAdmin = {
    /**
     * ThreeJS Options, coming from Drupal.settings.
     */
    threejsOptions: {},
    /**
     * Load ThreeJS once page is ready
     */
    attach: function (context, settings) {
      this.ThreeJSOptions = settings.threejs ? settings.threejs.options : {};

      var container = $('.canvas-container', context);
      if (container.length) {
        if (typeof THREE !== "undefined") {
          this.renderThreeJSField();
        }
        else {

        }
      }

    },

    /**
     * Simple threejs example for testing library
     */
    renderThreeJSField: function () {
      var settings = {
        fov: 1,
        aspectRatio: 4 / 5,
        cameraNear: 0.1,
        cameraFar: 50,
        cameraPosition: { x: -30, y: 35, z: 5 },
        backgroundColor: 0x8fbcd4
      }
      const objects = [];
      $(".canvas-container .threejs").each(function () {
        let canvas = $(this).get(0);
        init(canvas, $(this).data('loader'), $(this).data('model'));
      });

      function init(canvas, loaderName, model) {
        const scene = new THREE.Scene();
        scene.background = new THREE.Color(settings.backgroundColor);
        const clientRatio = canvas.clientWidth / canvas.clientHeight;
        const camera = new THREE.PerspectiveCamera(settings.fov, clientRatio, settings.cameraNear, settings.cameraFar);
        camera.position.set(settings.cameraPosition.x, settings.cameraPosition.y, settings.cameraPosition.z);
        const controls = new THREE.OrbitControls(camera, canvas);

        const lights = createLights();
        const materials = createMaterials();

        let loader = new THREE[loaderName]();
        loader.load(model, function (obj) {
          let mesh, traversable = false;
          let objBbox;
          switch (loaderName) {
            case 'STLLoader': {
              mesh = new THREE.Mesh(obj, materials);
              break;
            }
            default: {
              traversable = true;
              mesh = obj;  // Assume the loaded object is traversable
            }
          }

          objBbox = new THREE.Box3().setFromObject(mesh);

          // Calculate distance from camera to fit object.
          const boundingSphere = objBbox.getBoundingSphere(new THREE.Sphere());
          const objectSize = boundingSphere.radius;

          // Setup perspective camera.
          const cameraDistance = objectSize / Math.tan(Math.PI * 0.5 * camera.fov / 180);
          camera.position.copy(boundingSphere.center).add(new THREE.Vector3(0, 0, cameraDistance));
          camera.lookAt(boundingSphere.center);

          // Add OrbitControls so that we can pan around with the mouse.
          controls.enableDamping = true;
          controls.dampingFactor = 0.25;
          controls.enableZoom = true;
          controls.autoRotate = false;
          controls.autoRotateSpeed = 0.1;

          // Update size renderer.
          const { width, height } = canvas.getBoundingClientRect();
          camera.aspect = width / height;
          camera.updateProjectionMatrix();
          renderer.setSize(width, height);

          // Add object to scene.
          scene.add(mesh);
          scene.add(lights.ambient);
          scene.add(lights.main);
        });

        const renderer = createRenderer(canvas);

        setupOnWindowResize(camera, canvas, renderer);

        renderer.setAnimationLoop(() => {
          renderer.render(scene, camera);
        });
      }

      function createLights() {
        const ambient = new THREE.HemisphereLight(0xddeeaa, 0x0f0e0d, 2);

        const main = new THREE.DirectionalLight(0xbbbbbb, 3);
        main.position.set(1, 1, 1);

        return {
          ambient,
          main
        };
      }

      function createMaterials() {
        const main = new THREE.MeshStandardMaterial({
          color: 0xcccccc,
          flatShading: true,
          transparent: true,
          opacity: 0.5
        });

        main.color.convertSRGBToLinear();

        const highlight = new THREE.MeshStandardMaterial({
          color: 0xff4444,
          flatShading: true
        });

        highlight.color.convertSRGBToLinear();

        return {
          main,
          highlight
        };
      }

      function createRenderer(container) {
        let renderer = new THREE.WebGLRenderer({ canvas: container, antialias: true });
        renderer.setSize(container.clientWidth, container.clientHeight);

        renderer.setPixelRatio(window.devicePixelRatio);

        renderer.gammaFactor = 2.2;
        renderer.outputEncoding = THREE.sRGBEncoding;

        renderer.physicallyCorrectLights = true;

        return renderer;
      }

      function setupOnWindowResize(camera, container, renderer) {
        window.addEventListener("resize", () => {
          camera.aspect = container.clientWidth / container.clientHeight;
          camera.updateProjectionMatrix();
          renderer.setSize(container.clientWidth, container.clientHeight);
        });
      }
    }

  };

}(jQuery, Drupal));
