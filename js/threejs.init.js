(function ($,Drupal, settings, once) {
  Drupal.behaviors.ThreeJSinit = {
    attach: function (context) {
      $(once("initiate-threejs", ".canvas-container .threejs", context)).each(function () {
        const file = $(this).data('model');
        const extension = file.split('.').pop().toUpperCase();
        const loaderName = extension + 'Loader';
        let loader = new THREE[loaderName]();
        let canvas = $(this).get(0);
        let renderer = new THREE.WebGLRenderer({
          canvas: canvas,
          antialias: true
        });
        renderer.setPixelRatio(window.devicePixelRatio);

        let width = $(this).width();
        let height = $(this).height();
        let scene = new THREE.Scene();
        scene.background = new THREE.Color(settings.threejs.backgroundColor);
        let camera = new THREE.PerspectiveCamera(1, width / height, 0.1, 50);

        let ambient = new THREE.AmbientLight(0xffffff, 0.3);

        let keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
        keyLight.position.set(-100, 0, 100);

        let fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
        fillLight.position.set(100, 0, 100);

        let backLight = new THREE.DirectionalLight(0xffffff, 1.0);
        backLight.position.set(100, 0, -100).normalize();

        const texture = new THREE.TextureLoader().load(settings.threejs.SphereMapUrl);
        const material = new THREE.MeshPhongMaterial({
          color: 0xAAAAAA,
          specular: 0x111111,
          shininess: 200,
          map: texture
        });
        let mesh, objBbox, controls;
        loader.load(file, function (object) {
          let traversable = false;
          switch (extension) {
            case 'STL': {
              mesh = new THREE.Mesh(object, material);
              break;
            }
            default: {
              traversable = true;
              mesh = object;  // Assume the loaded object is traversable
            }
          }

          objBbox = new THREE.Box3().setFromObject(mesh);

          // Calculate distance from camera to fit object.
          const boundingSphere = objBbox.getBoundingSphere(new THREE.Sphere());
          const objectSize = boundingSphere.radius;
          const { width, height } = canvas.getBoundingClientRect();

          // Setup perspective camera.
          camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 1000);
          const cameraDistance = objectSize / Math.tan(Math.PI * 0.5 * camera.fov / 180);
          if(isNaN(settings.threejs.cameraX) && isNaN(settings.threejs.cameraY) && isNaN(settings.threejs.cameraZ)) {
            camera.position.set(settings.threejs.cameraX, settings.threejs.cameraY, settings.threejs.cameraZ);
          } else {
            camera.position.copy(boundingSphere.center).add(new THREE.Vector3(0, 0, cameraDistance));
            camera.lookAt(boundingSphere.center);
          }

          // Add OrbitControls so that we can pan around with the mouse.
          controls = new THREE.OrbitControls(camera, renderer.domElement);
          controls.enableDamping = true;
          controls.dampingFactor = 0.25;
          controls.enableZoom = true;
          controls.autoRotate = false;
          controls.autoRotateSpeed = 0.1;

          // Update size renderer.
          camera.aspect = width / height;
          camera.updateProjectionMatrix();
          renderer.setSize(width, height);

          // Add object to scene.
          scene.add(mesh);
          scene.add(ambient);
          scene.add(keyLight);
          scene.add(fillLight);
          scene.add(backLight);
        });

        render();

        function render() {
          // render using requestAnimationFrame
          requestAnimationFrame(render);
          // Autorotation Y axis.
          if (mesh) {
            if (!isNaN(settings.threejs.InitRotationX)) mesh.rotation.x -= settings.threejs.InitRotationX;
            if (!isNaN(settings.threejs.InitRotationY)) mesh.rotation.y -= settings.threejs.InitRotationY;
            if (!isNaN(settings.threejs.InitRotationZ)) mesh.rotation.z -= settings.threejs.InitRotationZ;
          }
          if(controls) {
            controls.update();  // update controls
          }
          renderer.render(scene, camera);
        }
      });
    }
  };
}(jQuery, Drupal, drupalSettings, once));
