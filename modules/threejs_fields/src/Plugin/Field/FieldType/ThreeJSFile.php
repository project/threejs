<?php

namespace Drupal\threejs_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'threejs_file' field type.
 */
#[FieldType(
  id: "threejs_file",
  label: new TranslatableMarkup("ThreeJS File"),
  description: new TranslatableMarkup("ThreeJS Files created by 3D Software."),
  category: "file_upload",
  default_widget: "threejs_file_widget",
  default_formatter: "threejs_file_formatter",
  list_class: FileFieldItemList::class,
  constraints: ["ReferenceAccess" => [], "FileValidation" => []],
  column_groups: [
    'file' => [
      'label' => new TranslatableMarkup('File'),
      'require_all_groups_for_translation' => TRUE,
      'columns' => ["target_id", "options"],
    ],
    'title' => [
      'label' => new TranslatableMarkup('Title'),
      'translatable' => TRUE,
    ],
  ],
)]
class ThreeJSFile extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'file_extensions' => 'obj dae glb gltf stl pdb 3dm 3mf amf bvh dds drc exr fbx ifc hdr gcode kmz ktx ktx2 lwo md2 mdd mmd mtl mpd nrrd pcd pdb ply prwm pvr rgbe rgbm svg tds tga tif tiff ttf tilt vox vrm vtk xyz usdz',
      'title_field' => 0,
      'title_field_required' => 0,
      'default_canvas' => [
        'uuid' => NULL,
        'title' => '',
        'options' => [
          'rotation' => [
            'x' => '0.01',
            'y' => '0.1',
          ],
          'camera' => [
            'position' => [
              'x' => '',
              'y' => '',
              'z' => '',
            ],
            'controls' => [
              'orbit',
            ],
          ],
        ],
      ],
    ] + parent::defaultFieldSettings();

    unset($settings['description_field']);
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    // Get base form from FileItem.
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = $this->getSettings();

    // Remove the description option.
    unset($element['description_field']);

    // Add title configuration options.
    $element['title_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable <em>Title</em> field'),
      '#default_value' => $settings['title_field'],
      '#description' => $this->t('The title attribute is used as a tooltip when the mouse hovers over the image. Enabling this field is not recommended as it can cause problems with screen readers.'),
      '#weight' => 11,
    ];
    $element['title_field_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('<em>Title</em> field required'),
      '#default_value' => $settings['title_field_required'],
      '#weight' => 12,
      '#states' => [
        'visible' => [
          ':input[name="settings[title_field]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['default_canvas'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Canvas options'),
      '#description' => $this->t("This default configurations for uploaded or created 3D Canvas objects."),
      '#open' => TRUE,
    ];
    // Convert the stored UUID to a FID.
    $fids = [];
    $uuid = $settings['default_canvas']['uuid'];
    if ($uuid && ($file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $uuid))) {
      $fids[0] = $file->id();
    }
    $element['default_canvas']['uuid'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Canvas object'),
      '#description' => $this->t('Canvas object to be shown if no Canvas object is uploaded.'),
      '#default_value' => $fids,
      '#upload_location' => $settings['uri_scheme'] . '://default_canvas/',
      '#element_validate' => [
        '\Drupal\file\Element\ManagedFile::validateManagedFile',
        [get_class($this), 'validateDefaultThreeJsFileForm'],
      ],
      '#upload_validators' => $this->getUploadValidators(),
    ];

    $element['default_canvas']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('The title attribute is used as a tooltip when the mouse hovers over the image.'),
      '#default_value' => $settings['default_canvas']['title'],
      '#maxlength' => 1024,
    ];
    $element['default_canvas']['options'] = [
      '#type' => 'details',
      '#title' => $this->t('3D Canvas options'),
      '#collapsed' => FALSE,
    ];
    // Animation settings.
    $element['default_canvas']['options']['rotation'] = [
      '#type' => 'item',
      '#title' => $this->t('Animation canvas object'),
      '#description' => $this->t('Animate Canvas object by X and Y (e.g. 0.1 and 0.001). Leave blank for no animation or using camera OrbitControls().'),
    ];
    $element['default_canvas']['options']['rotation']['x'] = [
      '#type' => 'number',
      '#title' => $this->t('Rotation X'),
      '#title_display' => 'invisible',
      '#default_value' => $settings['default_canvas']['options']['rotation']['x'],
      '#min' => 0,
      '#size' => 20,
      '#step' => 0.001,
      '#maxlength' => 24,
      '#field_suffix' => ' × ',
    ];
    $element['default_canvas']['options']['rotation']['y'] = [
      '#type' => 'number',
      '#title' => $this->t('Rotation Y'),
      '#title_display' => 'invisible',
      '#default_value' => $settings['default_canvas']['options']['rotation']['y'],
      '#min' => 0,
      '#size' => 20,
      '#step' => 0.001,
      '#maxlength' => 24,
      '#field_suffix' => ' ' . $this->t('milliseconds'),
    ];

    // Camera settings.
    $element['default_canvas']['options']['camera'] = [
      '#type' => 'details',
      '#title' => $this->t('Camera Options'),
      '#open' => TRUE,
    ];
    $element['default_canvas']['options']['camera']['position'] = [
      '#type' => 'item',
      '#title' => $this->t('Camera position settings'),
      '#description' => $this->t('Position and point the camera to the center of the scene.'),
    ];
    $element['default_canvas']['options']['camera']['position']['x'] = [
      '#type' => 'number',
      '#title' => $this->t('Camera position X'),
      '#title_display' => 'invisible',
      '#default_value' => $settings['default_canvas']['options']['camera']['position']['x'],
      '#maxlength' => 24,
      '#field_suffix' => $this->t('pixels'),
    ];
    $element['default_canvas']['options']['camera']['position']['y'] = [
      '#type' => 'number',
      '#title' => $this->t('Camera position Y'),
      '#title_display' => 'invisible',
      '#default_value' => $settings['default_canvas']['options']['camera']['position']['y'],
      '#maxlength' => 24,
      '#field_suffix' => $this->t('pixels'),
    ];
    $element['default_canvas']['options']['camera']['position']['z'] = [
      '#type' => 'number',
      '#title' => $this->t('Camera position Z'),
      '#title_display' => 'invisible',
      '#default_value' => $settings['default_canvas']['options']['camera']['position']['z'],
      '#maxlength' => 24,
      '#field_suffix' => $this->t('pixels'),
    ];

    // Camera Controls Settings.
    $element['default_canvas']['options']['camera']['controls'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Camera Control options'),
      '#options' => [
        'orbit' => $this->t('Orbit'),
        'fly' => $this->t('Fly'),
        'device_orientation' => $this->t('Device Orientation'),
        'drag' => $this->t('Drag'),
        'editor' => $this->t('Editor'),
        'first_person' => $this->t('First Person'),
        'pointer_lock' => $this->t('Pointer Lock'),
        'trackball' => $this->t('Trackball'),
        'transform' => $this->t('Transform'),
      ],
      '#default_value' => ['orbit'],
      '#description' => $this->t('Select camera Control parameters'),
    ];

    return $element;
  }

  /**
   * Validates the managed_file element for the default Image form.
   *
   * This function ensures the fid is a scalar value and not an array. It is
   * assigned as a #element_validate callback in
   * \Drupal\image\Plugin\Field\FieldType\ImageItem::defaultThreeJsFileForm().
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateDefaultThreeJsFileForm(array &$element, FormStateInterface $form_state) {
    // Consolidate the array value of this field to a single FID as #extended
    // for default image is not TRUE and this is a single value.
    if (isset($element['fids']['#value'][0])) {
      $value = $element['fids']['#value'][0];
      // Convert the file ID to an uuid.
      if ($file = \Drupal::service('entity_type.manager')->getStorage('file')->load($value)) {
        $value = $file->uuid();
      }
    }
    else {
      $value = '';
    }
    $form_state->setValueForElement($element, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function isDisplayed() {
    // Canvas items do not have per-item visibility settings.
    return TRUE;
  }

}
