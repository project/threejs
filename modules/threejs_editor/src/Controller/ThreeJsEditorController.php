<?php

namespace Drupal\threejs_editor\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Three.js Editor routes.
 */
class ThreeJsEditorController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
